import React from "react";

const Filter = (props) => {
  return (
    <div>
      <button className="px-5 py-2 m-2 bg-gray-200 rounded-lg">
        {props.name}
      </button>
    </div>
  );
};

export default Filter;
