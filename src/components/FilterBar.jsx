import React from "react";
import Filter from "./Filter";

const list = [
  "All",
  "Gaming",
  "Songs",
  "Live",
  "Soccer",
  "Cricket",
  "Cooking",
  "Basketball",
  "Tennis",
];

const FilterBar = () => {
  return (
    <div className="flex justify-center">
      {list.map((item, index) => {
        return <Filter name={item} key={index} />;
      })}
    </div>
  );
};

export default FilterBar;
