import React from "react";
import YoutubeLogo from "../assets/YoutubeLogo.png";
import MenuIcon from "../assets/MenuIcon.png";
import SearchIcon from "../assets/SearchIcon.png";
import Image from "next/image";
import { UserCircleIcon } from "@heroicons/react/solid";

const NavBar = () => {
  return (
    <div className="flex px-4 h-14 flex-row items-center justify-between">
      <div className="flex pl-3 cursor-pointer items-center pr-7 flex-row">
        <Image
          className="h-[24px] cursor-pointer w-[25px] mr-6"
          alt="menu"
          src={MenuIcon}
        />
        <Image className="h-5 w-[90px]" alt="youtube-logo" src={YoutubeLogo} />
      </div>
      <div className="flex flex-row items-center min-w-0 flex-grow-0 flex-shrink basis-[728px]">
        <input
          className="px-1 ml-10 flex-grow flex-shrink basis-[0.000000001px] h-10 border border-[#ccc] border-r-0 rounded-l-full pl-4 pr-1"
          type="text"
          placeholder="Search"
        />
        <button className="border border-[#ccc] p-2 rounded-r-full bg-[#f8f8f8] h-10 w-16 flex justify-center items-center">
          <Image
            className="w-[18px] h-[18px]"
            alt="SearchIcon"
            src={SearchIcon}
          />
        </button>
      </div>
      <div className="flex items-center justify-end min-w-[225px]">
        <UserCircleIcon className="h-8 w-8" />
      </div>
    </div>
  );
};

export default NavBar;
