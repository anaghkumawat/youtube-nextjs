import React from "react";
import FilterBar from "./FilterBar";
import VideoContainer from "./VideoContainer";

const MainContainer = () => {
  return (
    <div>
      <FilterBar />
      <VideoContainer />
    </div>
  );
};

export default MainContainer;
